/*
This file contains all of the code associated with the simulation and the 'overhead' stuff.
It does not contain any of the actual physics work (in nav-sto.cpp) or boundary conditions
It does however reference them in the increment() function
*/

#include "navier-stokes.hpp"

using namespace std;

/**
 * This function takes the current state of the navier-stokes object and converts it to a file. 
 * The file name is generated automatically as 2DexampleNavSto.csv.# where # is the number of times this function
 * has been called before (starting at 0).
 * 
 * Generates CSV files for use with Paraview.
 * */
void ns::createOutput(string filename)
{
	filename = filename + "." + to_string(outputNum);
	if (file_exists(filename))
	{
		cout << "The file \"" << filename << "\" already existed. It will be overwritten. " << endl;
	}
	ofstream myfile;
	myfile.open(filename);
	if (!myfile.is_open())
	{
		cout << "The file " << filename << "could not be created. " << endl;
		throw "The file " + filename + " could not be created. ";
	}
	myfile << "x,y,z,rho,v,vx,vy,vz,P,E\n";
	for (double xpos = 0; xpos < gridSize[0]; xpos++)
	{
		for (double ypos = 0; ypos < gridSize[1]; ypos++)
		{
			for (double zpos = 0; zpos < gridSize[2]; zpos++)
			{
				//TODO this can be much more efficient, but i can't be bothered atm.
				//how? I don't recall anymore
				double x = xMax * xpos / (double)(gridSize[0] - 1);
				double y = yMax * ypos / (double)(gridSize[1] - 1);
				double z = zMax * zpos / (double)(gridSize[2] - 1);
				if (isnan(x))
					x = 0;
				if (isnan(y))
					y = 0;
				if (isnan(z))
					z = 0;

				myfile << x << ",";
				myfile << y << ",";
				myfile << z << ",";
				myfile << rho[xpos][ypos][zpos] << ",";
				double v = sqrt(pow(vx[xpos][ypos][zpos], 2) + pow(vy[xpos][ypos][zpos], 2) + pow(vz[xpos][ypos][zpos], 2));
				myfile << v << ",";
				myfile << vx[xpos][ypos][zpos] << ",";
				myfile << vy[xpos][ypos][zpos] << ",";
				myfile << vz[xpos][ypos][zpos] << ",";
				myfile << P[xpos][ypos][zpos] << ",";
				myfile << E[xpos][ypos][zpos]; // <<",";
				myfile << endl;
			}
		}
	}
	myfile.close();
	outputNum++;
}

/**
 * The initialization function which creates all of the class variables which represent the current state of the system.
 * */
void ns::createVariables()
{
	//the number of grid points in the system, as defined in the constructor
	int nx = gridSize[0];
	int ny = gridSize[1];
	int nz = gridSize[2];

	//velocity is a set of vx, vy, vz variables at each point in space
	double vxi, vyi, vzi;
	vxi = vyi = vzi = 0.0; // m/s

	vx = matrix(nx, vector<vector<double>>(ny, vector<double>(nz, vxi)));
	vy = matrix(nx, vector<vector<double>>(ny, vector<double>(nz, vyi)));
	vz = matrix(nx, vector<vector<double>>(ny, vector<double>(nz, vzi)));

	v.push_back(vx);
	v.push_back(vy);
	v.push_back(vz);

	//Pressure at each point in space
	double Pi = 1.0; //Pa
	P = matrix(nx, vector<vector<double>>(ny, vector<double>(nz, Pi)));

	//Density at each point in space
	double rhoi = 1.0; // kg/m^3
	rho = matrix(nx, vector<vector<double>>(ny, vector<double>(nz, rhoi)));

	//Energy is calculated at the end of initialization
	E = matrix(gridSize[0], vector<vector<double>>(gridSize[1], vector<double>(gridSize[2], 0.0)));

	//it might be faster to create each top-level vector as a linear vector with size (xMax*yMax*zMax) and do arithmatic
	//to find positions, but think about that later

	/*
	//setting up air as part of the simulation
	for (int x = 0; x < nx; x++)
	{
		for (int y = 0; y < ny; y++)
		{
			for (int z = 0; z < nz; z++)
			{
				if (y > nx / 2)
				{
					rho[x][y][z] = 0.001; //air
					P[x][y][z] = 1.0;	  //air
				}
			}
		}
	}
	*/

	//change values of rho and P to be correct-ish with the value of gravity added in (there is a .1 atm difference between the surface and the bottom)
	//if gravity = 0, there will be no change
	for (int x = 0; x < nx; x++)
	{
		for (int y = 0; y < ny; y++)
		{
			for (int z = 0; z < nz; z++)
			{
				rho[x][y][z] = P[x][y][z] = exp(-1.0 * g / 100.0 * (1.0 - (double)y / (double)ny));
			}
		}
	}

	/*
	//this code is written to test out the simulation if a blob of high-density material is placed in the center of the simulation
	for(int x = 0;x<nx;x++){
		for(int y = 0;y<ny;y++){
			for(int z = 0;z<nz;z++){
				double fact =(1+.2*exp(-pow((double)(x-nx/2)/5.0,2) - pow((double)(y-ny/2)/5.0,2) - pow((double)(z-nz/2)/5.0,2)));
				rho[x][y][z] *= fact;
				P[x][y][z] *= fact;
			}
		}
	}
	*/

	//this code will insert a sheet of higher density material moving through the simulation
	for(int x = 1;x<nx-1;x++)
	{
		for(int z = 1;z<nz;z++){
			int y = 2;
			rho[x][y][z] = P[x][y][z] = 1.1;
			vy[x][y][z] = 1.0;
		}
	}
	

	updateEnergy();
	imposeBoundaryConditions();
}

/**
 * A short debugging function to print out variables in a similar way that they would be stored in the CSV files
 * */
void ns::printVariables()
{
	cout << "X Y Z" << endl;
	cout << "Velocity:" << endl;
	for (int xpos = 0; xpos != (int)vx.size(); xpos++)
	{
		for (int ypos = 0; ypos != (int)vx.at(0).size(); ypos++)
		{
			for (int zpos = 0; zpos != (int)vx.at(0).at(0).size(); zpos++)
			{
				cout << vx[xpos][ypos][zpos] << "," << vy[xpos][ypos][zpos] << "," << vz[xpos][ypos][zpos] << " ";
			}
			cout << endl;
		}
		cout << endl;
	}

	cout << "Pressure:" << endl;
	for (int xpos = 0; xpos != (int)P.size(); xpos++)
	{
		for (int ypos = 0; ypos != (int)P.at(0).size(); ypos++)
		{
			for (int zpos = 0; zpos != (int)P.at(0).at(0).size(); zpos++)
			{
				cout << P[xpos][ypos][zpos] << " ";
			}
			cout << endl;
		}
		cout << endl;
	}
}

void ns::increment()
{
	double dt = calculateTimeStep();
	if (dt > 1)
		dt = 1;
	increment(dt);
}

double ns::increment(double maxTimeStep)
{
	int startTime = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
	//TODO need to add some sort of output/debug file, so I know what the program does,
	//stuff like the timestep, calculation times, etc.
	double dt = calculateTimeStep();
	cout << left << "iteration: " << setw(5) << outputNum << " ";
	cout << left << "Maximum Timestep: " << setw(12) << dt << " ";

	double timeStep = dt/2;
	if (timeStep > maxTimeStep)
		timeStep = maxTimeStep;

	//stuff like the timestep, calculation times, etc.
	cout << "file: " << setw(5) << outputNum << "  ";
	cout << "timestep: " << timeStep << endl;

	//TODO: question: should this be done using product rule on the values inside the gradient?

	//I will try initially saying that P=E/V for the Nav-Sto equations
	//if this turns out to not work, then I will have to try stuff involving the equation of state

	//also, need to figure out which order to calculate time changes. All three of density, pressure, and velocity change,
	//and each use a different equation. In lim(dt->0), this isn't an issue, but could be for discrete time steps

	//should put all of the block of code below into a 'doDensityCalculation' function

	auto vals = vector<matrix *>();
	vals.push_back(&rho);
	vals.push_back(&vx);

	double scalar = -1;

	auto gradRhoX = gradientXReflectiveBoundary(vals, scalar);
	vals.pop_back();
	vals.push_back(&vy);
	auto gradRhoY = gradientYReflectiveBoundary(vals, scalar);
	vals.pop_back();
	vals.push_back(&vz);
	auto gradRhoZ = gradientZReflectiveBoundary(vals, scalar);

	#pragma omp parallel for
	for (int xpos = 0; xpos < gridSize[0]; xpos++)
	{
		for (int ypos = 0; ypos < gridSize[1]; ypos++)
		{
			for (int zpos = 0; zpos < gridSize[2]; zpos++)
			{
				rho[xpos][ypos][zpos] += timeStep * (gradRhoX[xpos][ypos][zpos] + gradRhoY[xpos][ypos][zpos] + gradRhoZ[xpos][ypos][zpos]);
			}
		}
	}

	//the velocity calculations are all very similar, but require a different version of the gradient function each time,
	//so I created static functions which call the class' gradient functions, and passed them in as function pointers
	doVelocityCalculation(vx, gradientXReflectiveBoundaryVel, timeStep);
	doVelocityCalculation(vy, gradientYReflectiveBoundaryVel, timeStep);
	doVelocityCalculation(vz, gradientZReflectiveBoundaryVel, timeStep);

	//very important for when the velocity calculations mess up
	imposeBoundaryConditions();

	doEnergyCalculation(timeStep);
	updateEnergy();
	//imposeBoundaryConditions();

	int currentTime = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
	cout << setw(20) << "program time: " << setw(6) << (double)(currentTime - programStart) / 1000.0 << " ";
	cout << setw(23) << "iteration took: " << setw(6) << (double)(currentTime - startTime) / 1000.0 << " ";
	double rettime = (double)(currentTime - startTime) / 1000.0;
	//lastIterationEnd = currentTime;
	cout << endl;

	return rettime;
}

/**
 * Imposes the correct boundary conditions. Currently these are: 
 * - velocity into the walls of the simulation are 0
 * */
void ns::imposeBoundaryConditions()
{
	int nx = gridSize[0];
	int ny = gridSize[1];
	int nz = gridSize[2];
	//vx = 0 at x=0, x= nx
	#pragma omp parallel
	#pragma omp for
	for (int ypos = 0; ypos < ny; ypos++)
	{
		for (int zpos = 0; zpos < nz; zpos++)
		{
			vx[0][ypos][zpos] = vy[0][ypos][zpos] = vz[0][ypos][zpos] = 0;
			vx[nx - 1][ypos][zpos] = vy[nx - 1][ypos][zpos] = vz[nx - 1][ypos][zpos] = 0;
		}
	}
	//vy = 0 at y=0, y= ny
	#pragma omp for
	for (int xpos = 0; xpos < nx; xpos++)
	{
		for (int zpos = 0; zpos < nz; zpos++)
		{
			vx[xpos][0][zpos] = vy[xpos][0][zpos] = vz[xpos][0][zpos] = 0;
			vx[xpos][ny - 1][zpos] = vy[xpos][ny - 1][zpos] = vz[xpos][ny - 1][zpos] = 0;
		}
	}
	//vz = 0 at z=0, z= nz
	#pragma omp for
	for (int xpos = 0; xpos < nx; xpos++)
	{
		for (int ypos = 0; ypos < ny; ypos++)
		{
			vx[xpos][ypos][0] = vy[xpos][ypos][0] = vz[xpos][ypos][0] = 0;
			vx[xpos][ypos][nz - 1] = vy[xpos][ypos][nz - 1] = vz[xpos][ypos][nz - 1] = 0;
		}
	}
}