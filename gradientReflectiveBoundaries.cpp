/*
This file contains all of the gradient functions assuming the boundaries act as walls, isolating the system from the outside world.  
*/

#include "navier-stokes.hpp"

using namespace std;

/**
 * This function takes in a set of variables (vals) and takes the gradient of the product of those values.
 * i.e. calculates \grad{scalar*product(vals)}
 * This is done using a central finite difference with error~O(h^2) with boundary values of 0. 
 * */
matrix ns::gradientXReflectiveBoundary(const vector<matrix *> &vals, const double &scalar)
{
	matrix retVal;
	//initialize retVal to be a matrix of 0s
	retVal = matrix(gridSize[0], vector<vector<double>>(gridSize[1], vector<double>(gridSize[2], 0)));

	//h = delta x
	double h = xMax / gridSize[0];

	//the values for the left and right sides are initialized to 0, so we don't need to set them. 

	//middle
	#pragma omp parallel for
	for (int xpos = 1; xpos < gridSize[0] - 1; xpos++)
	{
		for (int ypos = 0; ypos < gridSize[1]; ypos++)
		{
			for (int zpos = 0; zpos < gridSize[2]; zpos++)
			{
				//start by multiplying all of the values for (x+1) together to get val1_x+1 * val2_x+1 ...
				double valRight = 1;
				for (auto val : vals)
				{
					valRight *= (*val)[xpos + 1][ypos][zpos];
				}

				//get the values for (x-1)
				double valLeft = 1;
				for (auto val : vals)
				{
					valLeft *= (*val)[xpos - 1][ypos][zpos];
				}

				//get the sum of the val right, mid, left, and multiply by scalar/h^2
				retVal[xpos][ypos][zpos] = scalar / (h * 2) * (valRight - valLeft);
			}
		}
	}

	return retVal;
}

/**
 * This function takes in a set of variables (vals) and takes the gradient of the product of those values.
 * i.e. calculates \grad{scalar*product(vals)}
 * This is done using a central finite difference with error~O(h^2) with boundary values of 0. 
 * */
matrix ns::gradientYReflectiveBoundary(const vector<matrix *> &vals, const double &scalar)
{
	matrix retVal;
	//initialize retVal to be a matrix of 0s
	retVal = matrix(gridSize[0], vector<vector<double>>(gridSize[1], vector<double>(gridSize[2], 0)));


	//h = delta y
	double h = yMax / gridSize[1];

	//middle
	#pragma omp parallel for
	for (int xpos = 0; xpos < gridSize[0]; xpos++)
	{
		for (int ypos = 1; ypos < gridSize[1] - 1; ypos++)
		{
			for (int zpos = 0; zpos < gridSize[2]; zpos++)
			{
				//start by multiplying all of the values for (x+1) together to get val1_x+1 * val2_x+1 ...
				double valRight = 1;
				for (auto val : vals)
				{
					valRight *= (*val)[xpos][ypos + 1][zpos];
				}

				//get the values for (x-1)
				double valLeft = 1;
				for (auto val : vals)
				{
					valLeft *= (*val)[xpos][ypos - 1][zpos];
				}

				//get the sum of the val right, mid, left, and multiply by scalar/h^2
				retVal[xpos][ypos][zpos] = scalar / (h * 2) * (valRight - valLeft);
			}
		}
	}

	return retVal;
}

/**
 * This function takes in a set of variables (vals) and takes the gradient of the product of those values.
 * i.e. calculates \grad{scalar*product(vals)}
 * This is done using a central finite difference with error~O(h^2) with boundary values of 0.
 * */
matrix ns::gradientZReflectiveBoundary(const vector<matrix *> &vals, const double &scalar)
{
	matrix retVal;
	//initialize retVal to be a matrix of 0s
	retVal = matrix(gridSize[0], vector<vector<double>>(gridSize[1], vector<double>(gridSize[2], 0)));

	//h = delta z
	double h = zMax / gridSize[2];

	//middle
	#pragma omp parallel for
	for (int xpos = 0; xpos < gridSize[0]; xpos++)
	{
		for (int ypos = 0; ypos < gridSize[1]; ypos++)
		{
			for (int zpos = 1; zpos < gridSize[2]-1; zpos++)
			{
				//start by multiplying all of the values for (x+1) together to get val1_x+1 * val2_x+1 ...
				double valRight = 1;
				for (auto val : vals)
				{
					valRight *= (*val)[xpos][ypos][zpos + 1];
				}

				//get the values for (x-1)
				double valLeft = 1;
				for (auto val : vals)
				{
					valLeft *= (*val)[xpos][ypos][zpos - 1];
				}

				//get the sum of the val right, mid, left, and multiply by scalar/h^2
				retVal[xpos][ypos][zpos] = scalar / (h * 2) * (valRight - valLeft);
			}
		}
	}

	return retVal;
}

/**
 * This function takes in a set of variables (vals) and takes the gradient of the product of those values.
 * i.e. calculates \grad{scalar*product(vals)}
 * This is done using a central finite difference with error~O(h^2) with boundary values of 0. 
 * */
matrix ns::gradientReflectiveBoundary(const vector<matrix *> &vals, const double &scalar, const double &timeStep)
{
	matrix retVal(gridSize[0], vector<vector<double>>(gridSize[1], vector<double>(gridSize[2], 0)));

	auto gradRhoX = gradientXReflectiveBoundary(vals, scalar);
	auto gradRhoY = gradientYReflectiveBoundary(vals, scalar);
	auto gradRhoZ = gradientZReflectiveBoundary(vals, scalar);

	#pragma omp parallel for
	for (int xpos = 0; xpos < gridSize[0]; xpos++)
	{
		for (int ypos = 0; ypos < gridSize[1]; ypos++)
		{
			for (int zpos = 0; zpos < gridSize[2]; zpos++)
			{
				retVal[xpos][ypos][zpos] += timeStep * gradRhoX[xpos][ypos][zpos];
				retVal[xpos][ypos][zpos] += timeStep * gradRhoY[xpos][ypos][zpos];
				retVal[xpos][ypos][zpos] += timeStep * gradRhoZ[xpos][ypos][zpos];
			}
		}
	}

	return retVal;
}

matrix ns::gradientXReflectiveBoundaryVel(ns& navsto, const vector<matrix *> &val, const double &scalar)
{
	return navsto.gradientXReflectiveBoundary(val, scalar);
}
matrix ns::gradientYReflectiveBoundaryVel(ns &navsto, const vector<matrix *> &val, const double &scalar)
{
	return navsto.gradientYReflectiveBoundary(val, scalar);
}
matrix ns::gradientZReflectiveBoundaryVel(ns &navsto, const vector<matrix *> &val, const double &scalar)
{
	return navsto.gradientZReflectiveBoundary(val, scalar);
}
