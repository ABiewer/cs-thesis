#include "navier-stokes.hpp"

using namespace std;

/**
 * Constructor for the system. Currently creates a 3D space with 3 grid points in each direction
 * each side 1m, then creates the variables
 * 
 * */
ns::ns()
	: ns(20, 20, 20, 0.0)
{
}

ns::ns(int nx, int ny, int nz, double gravity)
{
	//nCores = thread::hardware_concurrency();
	//lastIterationEnd = 
	programStart = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
	outputNum = 0;
	gridSize.push_back(nx);
	gridSize.push_back(ny);
	gridSize.push_back(nz);
	xMax = 1;
	yMax = 1;
	zMax = 1;
	g = gravity;
	createVariables();
}

/**
 * No memory allocation yet. 
 * */
ns::~ns()
{
	return;
}

/**
 * Takes in a velocity component and the dimension's gradient function, and the scalar for the gradient function and runs the 
 * correct calculation on the velocity component.
 * */
void ns::doVelocityCalculation(matrix& vi, matrix (*gradFunc)(ns&, const vector<matrix *> &, const double &), const double& timeStep)
{

	//Is this necessary, we got the timestep from the function???
	//double timeStep = calculateTimeStep();

	auto vals = vector<matrix *>();
	vals.push_back(&vi);

	auto gradVI = gradFunc(*this, vals, 1.0);

	vals.clear();
	vals.push_back(&P);

	auto gradPI = gradFunc(*this, vals, 1.0);

	#pragma omp parallel for
	for (int xpos = 0; xpos < gridSize[0]; xpos++)
	{
		for (int ypos = 0; ypos < gridSize[1]; ypos++)
		{
			for (int zpos = 0; zpos < gridSize[2]; zpos++)
			{
				//I don't know why, but the values here are off by the timestep from what they should be, so I removed the factor from the equation below. 
				double deltaVI = -1.0 * timeStep * (vi[xpos][ypos][zpos] * gradVI[xpos][ypos][zpos] + gradPI[xpos][ypos][zpos] / rho[xpos][ypos][zpos]);
				if(&vi == &vy){
					deltaVI += timeStep * g; 
				}
				vi[xpos][ypos][zpos] += deltaVI;
			}
		}
	}
}

/**
 * 
 * */
void ns::doEnergyCalculation(double dt)
{
	//ef = ei - dt * (u dot (grad e) - e * (div u) )

	//grad e
	vector<matrix*> vals {&E};
	matrix dedx = gradientXReflectiveBoundary(vals, 1);
	matrix dedy = gradientYReflectiveBoundary(vals, 1);
	matrix dedz = gradientZReflectiveBoundary(vals, 1);

	//div u
	vals = {&vx};
	matrix dudx = gradientXReflectiveBoundary(vals, 1);
	vals = {&vy};
	matrix dudy = gradientYReflectiveBoundary(vals, 1);
	vals = {&vz};
	matrix dudz = gradientZReflectiveBoundary(vals, 1);

	//create an empty vector to plop in values. 
	matrix lhs (gridSize[0], vector<vector<double>>(gridSize[1], vector<double>(gridSize[2], 0)));
	#pragma omp parallel for
	for(int xpos = 0;xpos<gridSize[0];xpos++){
		for(int ypos = 0;ypos<gridSize[1];ypos++){
			for(int zpos = 0;zpos<gridSize[2];zpos++){
				lhs[xpos][ypos][zpos] = vx[xpos][ypos][zpos] * dedx[xpos][ypos][zpos]
									 + vy[xpos][ypos][zpos] * dedy[xpos][ypos][zpos]
									 + vz[xpos][ypos][zpos] * dedz[xpos][ypos][zpos];
				lhs[xpos][ypos][zpos] += E[xpos][ypos][zpos] * ( dudx[xpos][ypos][zpos] + 
																 dudy[xpos][ypos][zpos] + 
																 dudz[xpos][ypos][zpos]);
				lhs[xpos][ypos][zpos] *= -1.0 * dt; 
											 //lhs *= -1.0 * dt;
				E[xpos][ypos][zpos] += lhs[xpos][ypos][zpos];
				P[xpos][ypos][zpos] = E[xpos][ypos][zpos] * rho[xpos][ypos][zpos];
			}
		}
	}


}

/**
 * Calculates the largest time step that can be taken, such that the CFL condition is satisfied for C_max = 1;
 * Currently does this for every point in the space. 
 * */
double ns::calculateTimeStep()
{
	double dx = xMax / gridSize[0];
	double dy = yMax / gridSize[1];
	double dz = zMax / gridSize[2];
	//assume the best scenario
	double minStep = __DBL_MAX__;
	//CMAX is a value used to determine stability of the system. It is technically supposed to be <=1, but can be larger
	//if the system is well-behaved
	double CMAX = 1;
	#pragma omp parallel for
	for (int xpos = 0; xpos < gridSize[0]; xpos++)
	{
		for (int ypos = 0; ypos < gridSize[1]; ypos++)
		{
			for (int zpos = 0; zpos < gridSize[2]; zpos++)
			{
				//calculate the min timestep for each point in the grid
				double currentStep = CMAX / (abs(vx[xpos][ypos][zpos] / dx) + abs(vy[xpos][ypos][zpos] / dy) + abs(vz[xpos][ypos][zpos] / dz));
				if (currentStep < minStep)
				{
					//and change the global timestep if necessary
					minStep = currentStep;
				}
			}
		}
	}

	return minStep;
}

void ns::updateEnergy()
{
	#pragma omp parallel for
	for(int xpos = 0;xpos<gridSize[0];xpos++){
		for(int ypos = 0;ypos<gridSize[1]; ypos++){
			for(int zpos = 0;zpos<gridSize[2];zpos++){
				E[xpos][ypos][zpos] = P[xpos][ypos][zpos] / rho[xpos][ypos][zpos]; 	
				if(isnan(E[xpos][ypos][zpos]))
					E[xpos][ypos][zpos] = 0.0;
			}
		}
	}
}
