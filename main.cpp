#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <string>
#include "navier-stokes.hpp"
//PaCoSimFlo3D

using namespace std;

void readInFile(int &x, int &y, int &z, int &nRuns, double &maxTimeStep, double &gravity, string& prefix, string filename)
{
    string line;
    ifstream file(filename);
    while (getline(file, line))
    {
        try
        {
            if (line == "\n")
            {
                continue;
            }
            if (line.substr(0, 6) == "nRuns=")
                nRuns = stoi(line.substr(6));
            else if (line.substr(0, 2) == "x=")
                x = stoi(line.substr(2));
            else if (line.substr(0, 2) == "y=")
                y = stoi(line.substr(2));
            else if (line.substr(0, 2) == "z=")
                z = stoi(line.substr(2));
            else if (line.substr(0, 12) == "maxtimestep=")
                maxTimeStep = stod(line.substr(12));
            else if (line.substr(0, 8) == "gravity=")
                gravity = stod(line.substr(8));
            else if (line.substr(0,11) == "outputName=")
                prefix = line.substr(11);
        }
        catch (invalid_argument)
        {
            cout << "Invalid argument for input files: " << line << endl;
        }
        catch (out_of_range)
        {
            cout << "Value is too large: " << line << endl;
        }
    }
    file.close();
}

int main(int argc, char **argv)
{
    int nRuns, x, y, z;
    double maxTimeStep = 0.001;
    double gravity = -9.8;
    string prefix;
    if (argc > 1)
    {
        readInFile(x, y, z, nRuns, maxTimeStep, gravity, prefix, argv[1]);
    }
    else
    {
        readInFile(x, y, z, nRuns, maxTimeStep, gravity, prefix, "input.txt");
    }

    string filename = "output/"+prefix+".csv";

    if (file_exists(filename+".0"))
    {
        cout << "The file: "+ filename + ".0 already exists, would you like to overwrite it and any other existing files? (y or n): ";
        string line = "";
        while (line != "y")
        {
            getline(cin, line);
            if (line == "n")
                return 0;
            if (line != "y")
                cout << "Please enter either y or n: ";
        }
    }

    //cout<<omp_get_max_threads()<<endl;

    ns test(x, y, z, gravity);
    vector<double> times;
    for (int i = 0; i < nRuns; i++)
    {
        test.createOutput(filename);
        times.push_back(test.increment(maxTimeStep));
    }

    double average = 0;
    for(auto i : times)
        average += i;
    average /= (double)(times.size());
    cout<<"The average iteration took: "<<average<<" seconds. "<<endl;

    return 0;
}
