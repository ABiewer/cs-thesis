.PHONY: clean all

all: build run

build:
	g++ -Wall -std=c++17 -g *.cpp -fopenmp

build-release:
	g++ -Wall -std=c++17 -O3 *.cpp -fopenmp

run:
	./a.out
