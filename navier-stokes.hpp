#pragma once
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <cmath>
#include <algorithm>
//#include <functional>
#include <sys/stat.h>
#include <thread>
#include <iomanip>
#include <chrono>
#include <omp.h>

using namespace std;

typedef vector<vector<vector<double>>> matrix;

#ifndef _file_exists_function_
#define _file_exists_function_
/**
 * Used to verify that files are not being written to multiple times
 * */
inline bool file_exists(const string &name)
{
	struct stat buffer;
	return (stat(name.c_str(), &buffer) == 0);
}
#endif

class ns
{
public:
    //constructor/destructor
    ns();
    ns(int nx, int ny, int nz, double gravity);
    ~ns();

    //public functions
    void increment();
    double increment(double timeStep);
    void createOutput(string filename);

//private:
	//int nCores;
	int programStart;
	//int lastIterationEnd;
    //variables
    double g;        //gravity
    int outputNum;   //keeps track of the output files

    vector<int> gridSize; //for the number of grid points
    double xMax;          //the physical size of the simulation space (m)
    double yMax;          // """"""""
    double zMax;          // """"""""

    matrix rho; //density
    matrix vx;  //x velocity
    matrix vy;  //y velocity
    matrix vz;  //z velocity
    vector<matrix> v; //velocity components
	matrix E; //specific internal energy

    matrix P; //Pressure

    //functions
    void createVariables();     //setup
    void printVariables();      //debug
    double calculateTimeStep(); //timeStep

    void doVelocityCalculation(matrix& vi, matrix (*gradFunc)(ns&, const vector<matrix *>&, const double&), const double& timeStep);
	void doEnergyCalculation(double dt);

    //calculating the gradients,
    matrix gradientZeroBoundary(const vector<matrix *> &vals, const double &scalar, const double &timeStep);

    //helper functions for gradientZeroBoundary
    matrix gradientXZeroBoundary(const vector<matrix *> &val, const double &scalar);
    matrix gradientYZeroBoundary(const vector<matrix *> &val, const double &scalar);
    matrix gradientZZeroBoundary(const vector<matrix *> &val, const double &scalar);
    static matrix gradientXZeroBoundaryVel(ns& navsto, const vector<matrix *> &val, const double &scalar);
    static matrix gradientYZeroBoundaryVel(ns &navsto, const vector<matrix *> &val, const double &scalar);
    static matrix gradientZZeroBoundaryVel(ns &navsto, const vector<matrix *> &val, const double &scalar);
	
    //calculating the gradients,
    matrix gradientReflectiveBoundary(const vector<matrix *> &vals, const double &scalar, const double &timeStep);

    //helper functions for gradientZeroBoundary
    matrix gradientXReflectiveBoundary(const vector<matrix *> &val, const double &scalar);
    matrix gradientYReflectiveBoundary(const vector<matrix *> &val, const double &scalar);
    matrix gradientZReflectiveBoundary(const vector<matrix *> &val, const double &scalar);
    static matrix gradientXReflectiveBoundaryVel(ns& navsto, const vector<matrix *> &val, const double &scalar);
    static matrix gradientYReflectiveBoundaryVel(ns &navsto, const vector<matrix *> &val, const double &scalar);
    static matrix gradientZReflectiveBoundaryVel(ns &navsto, const vector<matrix *> &val, const double &scalar);
	
	//E is not maintained automatically, needs to be updated based on the current values of P and rho
	void updateEnergy();

    //The boundary conditions are not imposed automatically by the gradient functions or any calculations, so we need to nail them down so they don't move
    void imposeBoundaryConditions();
};