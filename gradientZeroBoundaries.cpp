/*
This file contains all of the gradient functions assuming no conditions exist outside of the simulation space. 
*/

#include "navier-stokes.hpp"

using namespace std;

/**
 * This function takes in a set of variables (vals) and takes the gradient of the product of those values.
 * i.e. calculates \grad{scalar*product(vals)}
 * This is done using a central finite difference with error~O(h^2) with boundary values of 0. 
 * */
vector<vector<vector<double>>> ns::gradientXZeroBoundary(const vector<vector<vector<vector<double>>> *> &vals, const double &scalar)
{
	vector<vector<vector<double>>> retVal;

	//construct the space for the retVal in memory
	retVal.reserve(gridSize[0]);
	for (int xpos = 0; xpos < gridSize[0]; xpos++)
	{
		retVal.push_back(vector<vector<double>>());
		retVal.at(xpos).reserve(gridSize[1]);
		for (int ypos = 0; ypos < gridSize[1]; ypos++)
		{
			retVal.at(xpos).push_back(vector<double>());
			retVal.at(xpos).at(ypos).reserve(gridSize[2]);
			for (int zpos = 0; zpos < gridSize[2]; zpos++)
			{
				retVal.at(xpos).at(ypos).push_back(0);
			}
		}
	}

	//h = delta x
	double h = xMax / gridSize[0];

	//left and right sides (x=0, xfinal)
	for (int ypos = 0; ypos < gridSize[1]; ypos++)
	{
		for (int zpos = 0; zpos < gridSize[2]; zpos++)
		{
			//start with x=0
			int xpos = 0;

			//start by multiplying all of the values for (x+1) together to get val1_x+1 * val2_x+1 ...
			double valRight = 1;
			for (auto val : vals)
			{
				valRight *= (*val)[xpos + 1][ypos][zpos];
			}

			//at x=0, there is no x-1, so the addition is 0
			double valLeft = 0;

			//get the sum of the val right, mid, left, and multiply by scalar/h^2
			retVal[xpos][ypos][zpos] = scalar / (h * 2) * (valRight + valLeft);

			//repeat the above for x = xfinal
			xpos = gridSize[0] - 1;

			//get the value for val1_x-1 * val2_x-1...
			valLeft = -1;
			for (auto val : vals)
			{
				valLeft *= (*val)[xpos - 1][ypos][zpos];
			}

			//no value for (x+1)
			valRight = 0;

			//do the same for x = xfinal
			retVal[xpos][ypos][zpos] = scalar / (h * 2) * (valRight + valLeft);
		}
	}

	//middle
	for (int xpos = 1; xpos < gridSize[0] - 1; xpos++)
	{
		for (int ypos = 0; ypos < gridSize[1]; ypos++)
		{
			for (int zpos = 0; zpos < gridSize[2]; zpos++)
			{
				//start by multiplying all of the values for (x+1) together to get val1_x+1 * val2_x+1 ...
				double valRight = 1;
				for (auto val : vals)
				{
					valRight *= (*val)[xpos + 1][ypos][zpos];
				}

				//get the values for (x-1)
				double valLeft = -1;
				for (auto val : vals)
				{
					valLeft *= (*val)[xpos - 1][ypos][zpos];
				}

				//get the sum of the val right, mid, left, and multiply by scalar/h^2
				retVal[xpos][ypos][zpos] = scalar / (h * 2) * (valRight + valLeft);
			}
		}
	}

	return retVal;
}

/**
 * This function takes in a set of variables (vals) and takes the gradient of the product of those values.
 * i.e. calculates \grad{scalar*product(vals)}
 * This is done using a central finite difference with error~O(h^2) with boundary values of 0. 
 * */
vector<vector<vector<double>>> ns::gradientYZeroBoundary(const vector<vector<vector<vector<double>>> *> &vals, const double &scalar)
{
	vector<vector<vector<double>>> retVal;

	//construct the space for the retVal in memory
	retVal.reserve(gridSize[0]);
	for (int xpos = 0; xpos < gridSize[0]; xpos++)
	{
		retVal.push_back(vector<vector<double>>());
		retVal.at(xpos).reserve(gridSize[1]);
		for (int ypos = 0; ypos < gridSize[1]; ypos++)
		{
			retVal.at(xpos).push_back(vector<double>());
			retVal.at(xpos).at(ypos).reserve(gridSize[2]);
			for (int zpos = 0; zpos < gridSize[2]; zpos++)
			{
				retVal.at(xpos).at(ypos).push_back(0);
			}
		}
	}

	//h = delta y
	double h = yMax / gridSize[0];

	//front and back sides (y=0, yfinal)
	for (int xpos = 0; xpos < gridSize[1]; xpos++)
	{
		for (int zpos = 0; zpos < gridSize[2]; zpos++)
		{
			//start with y=0
			int ypos = 0;

			//start by multiplying all of the values for (y+1) together to get val1_y+1 * val2_y+1 ...
			double valRight = 1;
			for (auto val : vals)
			{
				valRight *= (*val)[xpos][ypos + 1][zpos];
			}

			//at y=0, there is no y-1, so the addition is 0
			double valLeft = 0;

			//get the sum of the val right, mid, left, and multiply by scalar/h^2
			retVal[xpos][ypos][zpos] = scalar / (h * 2) * (valRight + valLeft);

			//repeat the above for y = yfinal
			ypos = gridSize[1] - 1;

			//get the value for val1_y-1 * val2_y-1...
			valLeft = -1;
			for (auto val : vals)
			{
				valLeft *= (*val)[xpos][ypos - 1][zpos];
			}

			//no value for (y+1)
			valRight = 0;

			//do the same for y = yfinal
			retVal[xpos][ypos][zpos] = scalar / (h * 2) * (valRight + valLeft);
		}
	}

	//middle
	for (int xpos = 1; xpos < gridSize[0] - 1; xpos++)
	{
		for (int ypos = 0; ypos < gridSize[1]; ypos++)
		{
			for (int zpos = 0; zpos < gridSize[2]; zpos++)
			{
				//start by multiplying all of the values for (x+1) together to get val1_x+1 * val2_x+1 ...
				double valRight = 1;
				for (auto val : vals)
				{
					valRight *= (*val)[xpos + 1][ypos][zpos];
				}

				//get the values for (x-1)
				double valLeft = -1;
				for (auto val : vals)
				{
					valLeft *= (*val)[xpos - 1][ypos][zpos];
				}

				//get the sum of the val right, mid, left, and multiply by scalar/h^2
				retVal[xpos][ypos][zpos] = scalar / (h * 2) * (valRight + valLeft);
			}
		}
	}

	return retVal;
}

/**
 * This function takes in a set of variables (vals) and takes the gradient of the product of those values.
 * i.e. calculates \grad{scalar*product(vals)}
 * This is done using a central finite difference with error~O(h^2) with boundary values of 0. 
 * */
vector<vector<vector<double>>> ns::gradientZZeroBoundary(const vector<vector<vector<vector<double>>> *> &vals, const double &scalar)
{
	vector<vector<vector<double>>> retVal;

	//construct the space for the retVal in memory
	retVal.reserve(gridSize[0]);
	for (int xpos = 0; xpos < gridSize[0]; xpos++)
	{
		retVal.push_back(vector<vector<double>>());
		retVal.at(xpos).reserve(gridSize[1]);
		for (int ypos = 0; ypos < gridSize[1]; ypos++)
		{
			retVal.at(xpos).push_back(vector<double>());
			retVal.at(xpos).at(ypos).reserve(gridSize[2]);
			for (int zpos = 0; zpos < gridSize[2]; zpos++)
			{
				retVal.at(xpos).at(ypos).push_back(0);
			}
		}
	}

	//h = delta z
	double h = zMax / gridSize[0];

	//top and bottom sides (z=0, zfinal)
	for (int xpos = 0; xpos < gridSize[1]; xpos++)
	{
		for (int ypos = 0; ypos < gridSize[2]; ypos++)
		{
			//start with y=0
			int zpos = 0;

			//start by multiplying all of the values for (z+1) together to get val1_z+1 * val2_z+1 ...
			double valRight = 1;
			for (auto val : vals)
			{
				valRight *= (*val)[xpos][ypos][zpos + 1];
			}

			//at z=0, there is no z-1, so the addition is 0
			double valLeft = 0;

			//get the sum of the val right, mid, left, and multiply by scalar/h^2
			retVal[xpos][ypos][zpos] = scalar / (h * 2) * (valRight + valLeft);

			//repeat the above for z = zfinal
			zpos = gridSize[2] - 1;

			//get the value for val1_z-1 * val2_z-1...
			valLeft = -1;
			for (auto val : vals)
			{
				valLeft *= (*val)[xpos][ypos][zpos - 1];
			}

			//no value for (z+1)
			valRight = 0;

			//do the same for z = zfinal
			retVal[xpos][ypos][zpos] = scalar / (h * 2) * (valRight + valLeft);
		}
	}

	//middle
	for (int xpos = 1; xpos < gridSize[0] - 1; xpos++)
	{
		for (int ypos = 0; ypos < gridSize[1]; ypos++)
		{
			for (int zpos = 0; zpos < gridSize[2]; zpos++)
			{
				//start by multiplying all of the values for (x+1) together to get val1_x+1 * val2_x+1 ...
				double valRight = 1;
				for (auto val : vals)
				{
					valRight *= (*val)[xpos + 1][ypos][zpos];
				}

				//get the values for (x-1)
				double valLeft = -1;
				for (auto val : vals)
				{
					valLeft *= (*val)[xpos - 1][ypos][zpos];
				}

				//get the sum of the val right, mid, left, and multiply by scalar/h^2
				retVal[xpos][ypos][zpos] = scalar / (h * 2) * (valRight + valLeft);
			}
		}
	}

	return retVal;
}

/**
 * This function takes in a set of variables (vals) and takes the gradient of the product of those values.
 * i.e. calculates \grad{scalar*product(vals)}
 * This is done using a central finite difference with error~O(h^2) with boundary values of 0. 
 * */
vector<vector<vector<double>>> ns::gradientZeroBoundary(const vector<vector<vector<vector<double>>> *> &vals, const double &scalar, const double &timeStep)
{
	vector<vector<vector<double>>> retVal;

	//construct the space for the retVal in memory
	retVal.reserve(gridSize[0]);
	for (int xpos = 0; xpos < gridSize[0]; xpos++)
	{
		retVal.push_back(vector<vector<double>>());
		retVal.at(xpos).reserve(gridSize[1]);
		for (int ypos = 0; ypos < gridSize[1]; ypos++)
		{
			retVal.at(xpos).push_back(vector<double>());
			retVal.at(xpos).at(ypos).reserve(gridSize[2]);
			for (int zpos = 0; zpos < gridSize[2]; zpos++)
			{
				retVal.at(xpos).at(ypos).push_back(0);
			}
		}
	}

	auto gradRhoX = gradientXZeroBoundary(vals, scalar);
	auto gradRhoY = gradientYZeroBoundary(vals, scalar);
	auto gradRhoZ = gradientZZeroBoundary(vals, scalar);

	for (int xpos = 0; xpos < gridSize[0]; xpos++)
	{
		for (int ypos = 0; ypos < gridSize[1]; ypos++)
		{
			for (int zpos = 0; zpos < gridSize[2]; zpos++)
			{
				retVal[xpos][ypos][zpos] += timeStep * gradRhoX[xpos][ypos][zpos];
				retVal[xpos][ypos][zpos] += timeStep * gradRhoY[xpos][ypos][zpos];
				retVal[xpos][ypos][zpos] += timeStep * gradRhoZ[xpos][ypos][zpos];
			}
		}
	}

	return retVal;
}

vector<vector<vector<double>>> ns::gradientXZeroBoundaryVel(ns& navsto, const vector<vector<vector<vector<double>>> *> &val, const double &scalar)
{
	return navsto.gradientXZeroBoundary(val, scalar);
}
vector<vector<vector<double>>> ns::gradientYZeroBoundaryVel(ns &navsto, const vector<vector<vector<vector<double>>> *> &val, const double &scalar)
{
	return navsto.gradientYZeroBoundary(val, scalar);
}
vector<vector<vector<double>>> ns::gradientZZeroBoundaryVel(ns &navsto, const vector<vector<vector<vector<double>>> *> &val, const double &scalar)
{
	return navsto.gradientZZeroBoundary(val, scalar);
}
