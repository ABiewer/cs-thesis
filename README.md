# Welcome to the Computer Science Senior Capstone of Adam Biewer.

This program is intended to be a basic computational physics simulation software capable of simulating the behavior of fluids in three dimensions. It implements the Navier-Stokes equations and (currently) forward Euler time stepping with boundary conditions assumptions for no-slip (v=0 at boundary). 

If you would like to run the program yourself, just type 'make' in the terminal where you download the files. You can also modify the file 'input.txt' to change how the simulation behaves. Currently, the initial conditions are coded into the 'createVariables()' function in 'simulation.cpp'. 